describe('tests for app.js', function() {

it('should return correct number items by start application', function() {
    //given
    var noAllItems, itemsToGuess, itemsLeftToGuess, level;

    //when
    noAllItems = model.getNoOfAllItems();
    itemsToGuess = model.getNoItemsToGuess();
    itemsLeftToGuess = model.getItemLeftToGuess();
    //level = model.getLevel();

    //then
    expect(noAllItems).toBe(4);
    expect(itemsToGuess).toBe(2);
    expect(itemsLeftToGuess).toBe(2);
    //expect(level).toBe(0);
  });

  it('should return proper values after hit on correct tile', function() {
      //given
      var success, noItemsToGuess, itemLeftToGuess, noAllItems, level, timeToHighlight, successGuess, lastHit;
      var numberToGuess = [];
      noOfAllItems=4,
      noItemsToGuess=2,
      itemLeftToGuess=2,
      successGuess=0,
      numberToGuess=[];

      numberToGuess.push(1);
      numberToGuess.push(1);
      numberToGuess.push(0);
      numberToGuess.push(0);

      //when
      success = model.hitItem(1);

      //then
      expect(success).toBe(true);

    });

  it('should generate element been called', function() {
    //given
    var ifControllerRun;
    spyOn(controller, generateElement);
    spyOn(view, addElement);
    spyOn(model, setValuesAfterAddItem);
    spyOn(model, generateItemsToGuess);
    spyOn(view, setNoOfAllItems);
    spyOn(model, getNoOfAllItems);
    spyOn(view, setNoOItemsToGuess);
    spyOn(model, getNoItemsToGuess);
    spyOn(view, setNoOfItemsLeftToGuess);
    spyOn(model, getItemLeftToGuess);
    spyOn(view, highlihtGeneratedItems);
    spyOn(model, getItemToGuess);
    spyOn(model, getTimeToHighlight);

    //when
    controller.generateElement();

    //then
    expect(controller.generateElement).toHaveBeenCalled();
});

it('should level be updated', function() {
    //given
    var level1;
    level1 = 0;

    //when
    level1 = model.setLevel();

    //then
    expect(level1).toBe(0);
  });
});

var view = (function () {
  var addElement = function (onClickFunction) {
    var div = document.createElement("DIV"),
      lastRow = document.getElementById('elements-to-quess'),
      elementNo = document.getElementsByClassName('item'),
      previous = parseInt(elementNo.length) + 1;
    div.onclick = onClickFunction;
    div.className = 'item';
    div.setAttribute('onclick', 'controller.hit(event)');
    div.setAttribute('id', previous.toString());
    div.setAttribute('selected', '');
    lastRow.appendChild(div);
  },
    setNoOfAllItems = function (noAllItems) {
      var element = document.getElementById('no-all-items');
      element.value = noAllItems;
    },
    setNoOItemsToGuess = function (noItemToGuess) {
      var element = document.getElementById('no-items-to-quess');
      element.value = noItemToGuess;
    },
    setNoOfItemsLeftToGuess = function (noItemLeft) {
      var element = document.getElementById('no-items-left-to-quess');
      element.value = noItemLeft;
    },
    getTimeToShow = function (highlightTime) {
      var element = document.getElementById('timeDropdown');
      return element.value;
    },
    getIndexSelectedOption = function (selectedOption) {
      var select = document.getElementById('timeDropdown');
      for (var i = 0; i < select.options.length; i++) {
        select.options[i].removeAttribute('selected');
        if (select.options[i].value == selectedOption) {
          select.selectedIndex = i;
        }
      }
      return select.selectedIndex;
    },
    setTimeOption = function (onClickFunction, highlightTime) {
      var select = document.getElementById('timeDropdown')
      var selectedIndex = getIndexSelectedOption(highlightTime);
      select.options[selectedIndex].setAttribute('selected', 'selected');
    },
    setNewLevel = function (level) {
      var element = document.getElementById('level');
      element.value = level;
    },
    getId = function (element) {
      return element.getAttribute('id');
    },
    highlihtGeneratedItems = function (numberToGuess, highlightTime) {
      var i, element;
      for (i = 0; i < numberToGuess.length; i++) {
        if (numberToGuess[i] == 1) {
          element = document.getElementById(parseInt(i + 1));
          highlihtOnBlue(element, highlightTime);
        }
        else {
          element = document.getElementById(parseInt(i + 1));
          resetColor(element, highlightTime);
        }
      }
      blockItems();
    },
    highlihtWhiteItems = function (numberToGuess) {
      var i, element;
      for (i = 0; i < numberToGuess.length; i++) {
        element = document.getElementById(parseInt(i + 1));
        resetColor(element);
      }
    },
    highlihtOnBlue = function (element, highlightTime) {
      element.style.backgroundColor = 'blue';
      setTimeout(function () {
        element.style.backgroundColor = 'white';
      }, highlightTime);
    },
    resetColor = function (element) {
      element.style.backgroundColor = 'white';
    },
    greenColor = function (element) {
      element.style.backgroundColor = 'green';
    },
    redColor = function (element, highlightTime) {
      element.style.backgroundColor = 'red';
      setTimeout(function () {
        element.style.backgroundColor = 'white';
      }, highlightTime);
    },
    blockItems = function (){
      document.getElementById('timeDropdown').disabled = true; 
      document.getElementById('setTime').disabled = true; 
      document.getElementById('addSquare').disabled = true; 
      document.getElementById('highlight').disabled = true; 
      setTimeout(function () {
        enableItems();
      }, 3000);

    },
    enableItems = function (){
      document.getElementById('timeDropdown').disabled = false; 
      document.getElementById('setTime').disabled = false; 
      document.getElementById('addSquare').disabled = false; 
      document.getElementById('highlight').disabled = false; 
    };
  return {
    addElement: addElement,
    setNoOfAllItems: setNoOfAllItems,
    setNoOItemsToGuess: setNoOItemsToGuess,
    setNoOfItemsLeftToGuess: setNoOfItemsLeftToGuess,
    getIndexSelectedOption: getIndexSelectedOption,
    getTimeToShow: getTimeToShow,
    setTimeOption: setTimeOption,
    setNewLevel: setNewLevel,
    getId: getId,
    highlihtOnBlue: highlihtOnBlue,
    resetColor: resetColor,
    highlihtGeneratedItems: highlihtGeneratedItems,
    redColor: redColor,
    greenColor: greenColor,
    highlihtWhiteItems: highlihtWhiteItems,
    enableItems: enableItems,
    blockItems: blockItems
  }
})();

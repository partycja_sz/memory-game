var controller = (function () {
  var startGame = function () {
    generateRandomItemsToHit();
  },
    hit = function (event) {
      var id;
      element = event.target;
      id = view.getId(element);
      if (model.hitItem(id) && (model.getLastHit(id) != id)) {
        view.setNoOfItemsLeftToGuess(model.getItemLeftToGuess());
        view.greenColor(element);
        model.setLastHit(id);
        if (model.getItemLeftToGuess() == 0) {
          setTimeout(function () {
            view.highlihtWhiteItems(model.getItemToGuess());
            setTimeout(function () {
              view.addElement(hit);
              model.setValuesAfterGuessAllItems();
              view.setNewLevel(model.getLevel());
              model.setLastHit(-1);
              generateRandomItemsToHit();
            }, 1000);
          }, 1000);
        }
      }
      else {
        view.redColor(element, model.getTimeToHighlight());
        setTimeout(function () {
          generateRandomItemsToHit();
        }, model.getTimeToHighlight());
        model.setLastHit(-1);
      }
    },
    generateElement = function () {
      view.addElement(hit);
      model.setValuesAfterAddItem();
      generateRandomItemsToHit();
    },
    generateRandomItemsToHit = function () {
      model.generateItemsToGuess();
      view.setNoOfAllItems(model.getNoOfAllItems());
      view.setNoOItemsToGuess(model.getNoItemsToGuess());
      view.setNoOfItemsLeftToGuess(model.getItemLeftToGuess());
      view.highlihtGeneratedItems(model.getItemToGuess(), model.getTimeToHighlight());
    },
    setTimeOption = function () {
      model.setTimeToHighlight(view.getTimeToShow());
      view.setTimeOption(model.getTimeToHighlight());
    };
  return {
    startGame: startGame,
    hit: hit,
    generateElement: generateElement,
    generateRandomItemsToHit: generateRandomItemsToHit,
    setTimeOption: setTimeOption
  }
})();

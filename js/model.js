var model = (function () {
  var noOfAllItems = 4,
    noItemsToGuess = 2,
    itemLeftToGuess = 2,
    level = 0,
    timeToHighlight = 3000,
    successGuess = 0,
    numberToGuess = [],
    lastHit = -1

  getNoItemsToGuess = function () {
    return noItemsToGuess;
  },
    setNoItemsToGuess = function () {
      if ((noItemsToGuess / noOfAllItems) <= 0.3) {
        noItemsToGuess++;
      }
      return noItemsToGuess;
    },
    getLevel = function () {
      return level;
    },
    setLevel = function () {
      return level++;
    },
    getItemLeftToGuess = function () {
      return itemLeftToGuess;
    },
    setItemLeftToGuess = function () {
      return itemLeftToGuess--;
    },
    setNoOfAllItems = function () {
      return noOfAllItems++;
    },
    getNoOfAllItems = function () {
      return noOfAllItems;
    },
    getSuccessGuess = function () {
      return successGuess;
    },
    setSuccessGuess = function () {
      return successGuess++;
    },
    resetValues = function () {
      successGuess = 0;
      itemLeftToGuess = noItemsToGuess;
      numberToGuess = [];
    },
    generateItemsToGuess = function () {
      var i, idToGuess;
      existingNmbers = [];
      lastHit = -1
      resetValues();
      do {
        idToGuess = Math.floor(Math.random() * getNoOfAllItems() + 1);
        if (existingNmbers.indexOf(idToGuess) == -1) {
          existingNmbers.push(idToGuess);
        }
      }
      while (existingNmbers.length < getNoItemsToGuess());
      existingNmbers.sort();
      for (i = 1; i <= getNoOfAllItems(); i++) {
        if (existingNmbers.includes(i)) {
          numberToGuess.push(1);
        }
        else {
          numberToGuess.push(0);
        }
      }
    },
    getItemToGuess = function () {
      return numberToGuess;
    },
    hitItem = function (id) {
      if (numberToGuess[id - 1] == 1) {
        setSuccessGuess();
        setItemLeftToGuess();
        return true;
      }
      else {
        resetValues();
        return false;
      }
    },
    setValuesAfterAddItem = function () {
      setNoOfAllItems();
      setNoItemsToGuess();
      resetValues();
    },
    setValuesAfterGuessAllItems = function () {
      setLevel();
      setValuesAfterAddItem();
    },
    setLastHit = function (currentHit) {
      lastHit = currentHit;
    },
    getLastHit = function () {
      return lastHit;
    },
    getTimeToHighlight = function () {
      return timeToHighlight;
    },
    setTimeToHighlight = function (timeToShow) {
      timeToHighlight = timeToShow;
    };
  return {
    resetValues: resetValues,
    generateItemsToGuess: generateItemsToGuess,
    hitItem: hitItem,
    getTimeToHighlight: getTimeToHighlight,
    setTimeToHighlight: setTimeToHighlight,
    setValuesAfterAddItem: setValuesAfterAddItem,
    setValuesAfterGuessAllItems: setValuesAfterGuessAllItems,
    getItemToGuess: getItemToGuess,
    getNoOfAllItems: getNoOfAllItems,
    setNoOfAllItems: setNoOfAllItems,
    setNoItemsToGuess: setNoItemsToGuess,
    getNoItemsToGuess: getNoItemsToGuess,
    setItemLeftToGuess: setItemLeftToGuess,
    getItemLeftToGuess: getItemLeftToGuess,
    getLevel: getLevel,
    setLevel: setLevel,
    getSuccessGuess: getSuccessGuess,
    setSuccessGuess: setSuccessGuess,
    setLastHit: setLastHit,
    getLastHit: getLastHit
  }
})();
